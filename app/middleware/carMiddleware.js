// Get all car
const getAllCarMiddleware = (req, res, next) => {
    console.log("Get All Car!");
    next();
};
// get a car
const getCarMiddleware = (req, res, next) => {
    console.log("Get a Car!");
    next();
};
// Create new car
const createCarMiddleware = (req, res, next) => {
    console.log("Create new Car!");
    next();
};
// update a car
const updateCarMiddleware = (req, res, next) => {
    console.log("Update a Car!");
    next();
};
// delete a car
const deleteCarMiddleware = (req, res, next) => {
    console.log("Delete a Car!");
    next();
};

module.exports  = {
    getAllCarMiddleware,
    getCarMiddleware,
    createCarMiddleware,
    updateCarMiddleware,
    deleteCarMiddleware
}
