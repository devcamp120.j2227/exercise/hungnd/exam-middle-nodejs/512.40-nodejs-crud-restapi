// get all user
const getAllUserMiddleware = (req, res, next) => {
    console.log("Get All User!");
    next();
};
// get a user
const getUserMiddleware = (req, res, next) => {
    console.log("Get a User!");
    next();
};
// Create new user
const createUserMiddleware = (req, res, next) => {
    console.log("Create new User!");
    next();
};
// Update user
const updateUserMiddleware = (req, res, next) => {
    console.log("Update a User!");
    next();
};
// Delete a user
const deleteUserMiddleware = (req, res, next) => {
    console.log("Delete a User!");
    next();
};

module.exports  = {
    getAllUserMiddleware,
    getUserMiddleware,
    createUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}
