// khai báo thư viện express
const express = require('express');

// Import các middleware
const carMiddeware = require('../middleware/carMiddleware');

// Tạo ra Router
const carRouter = express.Router();

// Import carController
const carController = require('../controllers/carController');

// Create new car
carRouter.post('/users/:userId/cars', carMiddeware.createCarMiddleware, carController.createCar);

// Get all car
carRouter.get('/cars', carMiddeware.getAllCarMiddleware, carController.getAllCar);

// Get a car by id
carRouter.get('/cars/:carId', carMiddeware.getCarMiddleware, carController.getCarById);

// Update a car by id
carRouter.put('/cars/:carId', carMiddeware.updateCarMiddleware, carController.updateCarById);

// Delete a car by id
carRouter.delete('/cars/:carId', carMiddeware.deleteCarMiddleware, carController.deleteCarById);

// Get Car Of User
carRouter.get('/users/:userId/cars', carController.getAllCarOfUser);

module.exports = { carRouter }