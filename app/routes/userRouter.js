// khai báo thư viện express
const express = require('express');

// Import userMiddleware
const userMiddleware = require('../middleware/userMiddleware');

// Tạo ra Router
const userRouter = express.Router();

// Import userController
const userController = require('../controllers/userController');

// Create new user
userRouter.post('/users', userMiddleware.createUserMiddleware, userController.createUser);

// Get all user
userRouter.get('/users', userMiddleware.getAllUserMiddleware, userController.getAllUser);

// Get a user by id
userRouter.get('/users/:userId', userMiddleware.getUserMiddleware, userController.getUserById);

// Update a user by id
userRouter.put('/users/:userId', userMiddleware.updateUserMiddleware, userController.updateUserById);

// Delete a user by id
userRouter.delete('/users/:userId', userMiddleware.deleteUserMiddleware, userController.deleteUserById);

module.exports = { userRouter }