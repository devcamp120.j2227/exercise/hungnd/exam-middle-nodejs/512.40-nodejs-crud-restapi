
// Khái báo thư viện mongooseJS
const mongoose = require("mongoose");
// khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    age: {
        type: Number,
        default: 0
    },
    cars: [{
        type: mongoose.Types.ObjectId,
        ref: "Car"
    }]
}, {
    timestamps: true
});
module.exports = mongoose.model("User", userSchema);