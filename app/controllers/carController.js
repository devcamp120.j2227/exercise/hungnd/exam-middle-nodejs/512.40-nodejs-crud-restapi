// Khai báo thư viện mongoose
const mongoose = require("mongoose");

// Khai báo carModel
const carModel = require('../model/carModel');
const userModel = require("../model/userModel");
const { updateUserById } = require('./userController');
// create new car
const createCar = (req, res) => {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId;
    console.log(userId);

    let bodyCar = req.body;
    console.log(bodyCar);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra userI
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid!"
        });
    }
    // Kiểm tra model
    if (!bodyCar.model) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Model is not valid!"
        });
    }
    // Kiểm tra vId
    if (!bodyCar.vId) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "vId is not valid!"
        })
    }
    //B3: thực hiện tạo mới car
    const newCar = {
        _id: mongoose.Types.ObjectId(),
        model: bodyCar.model,
        vId: bodyCar.vId
    }
    carModel.create(newCar, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        // Thêm ID của car mới được tạo vào trong mảng cars của user truyền tại params
        userModel.findByIdAndUpdate(userId, { $push: { cars: data._id } }, (err, updateUserById) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Create new car successfully!",
                    data: data
                })
            }
        })
    })
};

// get all car
const getAllCar = (req, res) => {
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện load all car
    carModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Get all car successfully!",
                data: data
            })
        }
    })
};

// get a car by id
const getCarById = (req, res) => {
    //B1: Thu thập dữ liệu
    let carId = req.params.carId;
    //B2: Kiểm tra dữ liệu
    // Kiểm tra carId
    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "carId is not valid!"
        })
    }
    // B3: Thực hiện load car theo id
    carModel.findById(carId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Get car by Id successfully!",
                data: data
            })
        }
    })
};

// update car by Id
const updateCarById = (req, res) => {
    //B1: Thu thập dữ liệu
    let carId = req.params.carId;
    console.log(carId);

    let bodyCar = req.body;
    console.log(bodyCar);

    //B2: Kiểm tra dữ liệu
    // Kiểm tra model
    if (!bodyCar.model) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Model is not valid!"
        });
    }
    // Kiểm tra vId
    if (!bodyCar.vId) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "vId is not valid!"
        })
    }
    // B3: Thực hiện update car theo id
    let newCar = {
        model: bodyCar.model,
        vId: bodyCar.vId
    }
    carModel.findByIdAndUpdate(carId, newCar, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update car by Id successfully!",
                data: data
            })
        }
    })
};

// delete a car by Id
const deleteCarById = (req, res) => {
    //B1: Thu thập dữ liệu
    let carId = req.params.carId;
    console.log(carId);
    // B2: Kiểm tra dữ liệu
    // KIểm tra carId
    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "carId is not valid!"
        })
    }
    // B3: Thực hiện xóa car theo id
    carModel.findByIdAndDelete(carId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Delete car by Id successfully!",
                data: data
            })
        }
    })
};

// Get all Car Of User
const getAllCarOfUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User ID is not valid!"
        })
    }
    //B3: Thực hiện khởi tạo dữ liệu
    userModel
        .findById(userId)
        .populate("cars")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "Get all car of user successfully!",
                    "data": data
                })
            }
        })
};


module.exports = {
    createCar,
    getAllCar,
    getCarById,
    updateCarById,
    deleteCarById,
    getAllCarOfUser
}
