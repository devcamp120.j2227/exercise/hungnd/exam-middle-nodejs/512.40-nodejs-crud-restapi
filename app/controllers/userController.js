// Khai báo thư viện mongoose
const mongoose = require("mongoose");

// Khai báo userModel
const userModel = require('../model/userModel');

// create a user
const createUser = (req, res) => {
    //B1: Thu thập dữ liệu
    let bodyUser = req.body;
    console.log(bodyUser);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra name
    if (!bodyUser.name) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Name is not valid!"
        });
    }
    // Kiểm tra phone
    if (!bodyUser.phone || !validatePhone(bodyUser.phone)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Phone is not valid!"
        })
    }
    // Kiểm tra age
    if (!bodyUser.age || bodyUser.age < 0 || bodyUser.age > 200) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Age is not valid!"
        });
    }
    //B3: thực hiện tạo mới user
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        name: bodyUser.name,
        phone: bodyUser.phone,
        age: bodyUser.age
    }
    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Create new user successfully!",
                data: data
            })
        }
    })
};

// get all user
const getAllUser = (req, res) => {
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện load all user
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Load all user successfully!",
                data: data
            })
        }
    })
};

// get a user by id
const getUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "User Id is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    userModel.findById(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Get user by Id successfully!",
                data: data
            })
        }
    })
};

// update user by Id
const updateUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId;
    console.log(userId);

    let bodyUser = req.body;
    console.log(bodyUser);

    //B2: Kiểm tra dữ liệu
    // KIểm tra  userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "User Id is not valid!"
        })
    }
    // Kiểm tra phone
    if (!bodyUser.phone || !validatePhone(bodyUser.phone)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Phone is not valid!"
        })
    }
    // Kiểm tra age
    if (!bodyUser.age || bodyUser.age < 0 || bodyUser.age > 200) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Age is not valid!"
        });
    }
    // B3: Thực hiện update course theo id
    let newUser = {
        name: bodyUser.name,
        phone: bodyUser.phone,
        age: bodyUser.age
    }
    userModel.findByIdAndUpdate(userId, newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update user by Id successfully!",
                data: data
            })
        }
    })
};

// delete a user by Id
const deleteUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId;
    console.log(userId);
    // B2: Kiểm tra dữ liệu
    // KIểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "User Id is not valid!"
        })
    }
    // B3: Thực hiện xóa user theo id
    userModel.findByIdAndDelete(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Delete user by Id successfully!",
                data: data
            })
        }
    })
};

// Hàm kiểm tra định dạng số điện thoại
const validatePhone = (phone) => {
    "use strict";
    var vform = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vform.test(phone)) {
        return true;
    }
    else {
        return false;
    }
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}
